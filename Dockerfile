FROM php:7.0-apache
RUN mkdir /DATATUS
COPY assets/ports.conf /etc/apache2/ports.conf
COPY src/ /var/www/html/
EXPOSE 8080
USER 12345
